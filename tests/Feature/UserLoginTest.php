<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use App\Models\User;

class UserLoginTest extends TestCase
{
    use RefreshDatabase;

    /**
     * 未認証の場合のリダイレクト処理が機能しているか
     */
    public function testGetPageNotAuthenticated():void
    {
        $response = $this->get('/home');
        $response->assertRedirect('/login');
    }

    /**
     * ログイン機能が正常に行われているか
     */
    public function testLogin():void
    {
        $this->createAuthenticatedUser()->get('/home')->assertSuccessful();
    }

    /**
     * モックユーザー作成
     * @return UserLoginTest
     */
    private function createAuthenticatedUser():UserLoginTest{
        $user = factory(User::class)->create();
        return $this->actingAs($user);
    }
}
