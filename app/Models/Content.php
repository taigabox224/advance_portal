<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $guarded = [
        'id'
    ];

    protected $hidden = [
        'product_key'
    ];

}
