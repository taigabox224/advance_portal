<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Content;
use Illuminate\Auth\Access\HandlesAuthorization;

class ContentPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @param Content $content
     * @return bool
     */
    public function update(User $user, Content $content):bool
    {
        return $user->id === $content->user_id;
    }

    /**
     * @param User $user
     * @param Content $content
     * @return bool
     */
    public function delete(User $user, Content $content):bool
    {
        return $user->id === $content->user_id;
    }

    /**
     * @param User $user
     * @param Content $content
     * @return bool
     */
    public function view(User $user, Content $content):bool
    {
        return $user->id === $content->user_id;
    }
}
