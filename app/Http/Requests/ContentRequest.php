<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => 'filled|mimes:zip',
            'release_days' => 'required|integer|min:0',
            'name' => 'required|string',
            'description' => 'string',
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'file' => 'アップロードファイル',
            'release_days' => '解禁日数',
            'name' => 'コンテンツ名',
            'description' => 'コンテンツ概要',
        ];
    }
}
