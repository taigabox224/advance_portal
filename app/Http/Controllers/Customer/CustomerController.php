<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Repositories\Customer\CustomerInterface;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\RedirectResponse;

class CustomerController extends Controller
{
    private $repository;
    private $loginUser;

    /**
     * CustomerController constructor.
     * @param CustomerInterface $repository
     */
    public function __construct(CustomerInterface $repository)
    {
        $this->repository = $repository;
        $this->middleware(function ($request, $next) {
            $this->loginUser = \Auth::user();
            return $next($request);
        });
    }

    /**
     * @param Request $request
     * @return View
     */
    public function index():View
    {
        $customers = $this->repository->all($this->loginUser->domain);
        if ($customers === ['data' => []]) return back()->with('failure', __('messages.failure', ['target' => '顧客情報の取得']));
        return view('customers.index', ['customers' => $customers]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function search(Request $request):View
    {
        $inputs = $request->all();
        $customers = $this->repository->search($inputs, $this->loginUser->domain);
        $request->session()->flash('old', $inputs);
        return view('customers.index', ['customers'=> $customers]);
    }

    /**
     * @param int $id
     * @return View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function show(int $id):View
    {
        $customer = $this->repository->find($id, $this->loginUser->domain);
        return view('customers.show', ['customer' => $customer['data']]);
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function edit(int $id):View
    {
        $customer = $this->repository->find($id, $this->loginUser->domain);
        return view('customers.edit', ['customer' => $customer['data']]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function update(Request $request):RedirectResponse
    {
        $inputs = $request->all();
        $this->repository->update($inputs, $this->loginUser->domain);
        return redirect(route('customer.show', ['customer' => $inputs['id']]))->with('success', __('messages.success', ['target' => '顧客情報の更新']));
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(int $id)
    {
        $this->repository->destroy($id, $this->loginUser->domain);
        return redirect(route('customer.index', ))->with('success', __('messages.success', ['target' => '顧客情報の削除']));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function getArchivedList()
    {
        $customers = $this->repository->getArchivedList($this->loginUser->domain);
        return view('customers.index', ['customers' => $customers, 'archived' => true]);
    }

    /**
     *
     */
    public function unarchive()
    {

    }
}
