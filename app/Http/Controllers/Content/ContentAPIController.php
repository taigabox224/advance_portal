<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\Content as ContentResource;
use App\Models\Content;
use Carbon\Carbon;

class ContentAPIController extends Controller
{
    /**
     * @param Request $request
     * @param Content $content
     * @return ContentResource
     */
    public function content(Request $request, Content $content):ContentResource
    {
        return new ContentResource($content->find($request->id));
    }

    /**
     * @param Request $request
     * @param Content $content
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws \Exception
     */
    public function contentCollection(Request $request, Content $content):\Illuminate\Http\Resources\Json\AnonymousResourceCollection
    {
        $contents = $content->where('user_id', $request->user_id)->where('release_days', '<', $this->getElapsedDays($request))->get();
        return ContentResource::collection($contents);
    }

    /**
     * @param object $request
     * @return float
     * @throws \Exception
     */
    private function getElapsedDays(object $request):float
    {
        $elapsedSeconds = time() - (new Carbon($request->service_start_date))->getTimestamp() + $this->convertSecondsToDays($request->bonus_point);
        return ceil($elapsedSeconds / 60 / 60 / 24);
    }

    /**
     * @param $seconds
     * @return float
     */
    private function convertSecondsToDays(float $seconds):float
    {
        return ceil($seconds * 60 * 60 * 24);
    }

}
