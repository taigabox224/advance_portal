<?php

namespace App\Http\Controllers\Content;

use App\Models\Content;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\ContentRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ContentController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $contents = Content::where('user_id', Auth::id())->get();
        return view('contents.index', ['contents' => $contents]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create():\Illuminate\View\View
    {
        return view('contents.create');
    }

    /**
     * @param ContentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ContentRequest $request):\Illuminate\Http\RedirectResponse
    {
        $unixTime = Carbon::now()->getTimestamp();
        $userId = Auth::id();
        $productKey = (string)Str::uuid(); //プロダクトキーの生成

        $originalFileName = Str::of($request->file('file')->getClientOriginalName())->replace('.zip', '');
        $fileName = "{$originalFileName}_{$unixTime}.zip";
        $url = $request->file('file')->storeAs("public/contents/{$userId}", $fileName);

        $content = new Content();
        $content->fill($request->except('file'));
        $content->fill(['user_id' => $userId]);
        $content->fill(['product_key' => $productKey]);
        $content->fill(['content_url' => $url]);

        $content->save();
        return redirect(route('content.index'))->with('status', ['content' => 'コンテンツをアップロードしました', 'message_type' => 'success']);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show($id):\Illuminate\View\View
    {
        $content = Content::find($id);
        $this->authorize('view', $content);
        return view('contents.show', ['content' => $content]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit($id)
    {
        $content = Content::find($id);
        $this->authorize('update', $content);
        return view('contents.edit', ['content' => $content]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(ContentRequest $request, $id)
    {
        $content = Content::find($id);
        $this->authorize('update', $content);
        $content->fill($request->all())->save();
        return redirect(route('content.index'))->with('status', ['content' => 'コンテンツを更新しました', 'message_type' => 'success']);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy($id)
    {
        $content = Content::find($id);
        $this->authorize('delete', $content);
        Storage::delete($content->content_url);
        $content->delete();
        return redirect(route('content.index'))->with('status', ['content' => 'コンテンツを削除しました', 'message_type' => 'success']);
    }
}

