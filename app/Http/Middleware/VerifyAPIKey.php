<?php

namespace App\Http\Middleware;

use Closure;

class VerifyAPIKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(config('app.subway_api_key') !== $request->subway_api_key)
        {
            return response()->json([
                'status_code' => 401,
                'message' => 'you need Subway API Key'
            ]);
        };
        return $next($request);
    }
}
