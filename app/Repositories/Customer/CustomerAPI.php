<?php

namespace App\Repositories\Customer;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\RequestOptions;

/**
 * APIへの顧客情報取得リクエストの責務を負う
 * Class CustomerAPI
 * @package App\Repositories\Customer
 */
class CustomerAPI implements CustomerInterface
{
    /**
     * @param string $domain
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function all(string $domain): array
    {
        $url = "{$domain}/api/user/collection";
        $method = "POST";

        try {
            $client = new Client(
                [RequestOptions::VERIFY => FALSE]
            );
            $jsonCustomers = $client->request(
                $method,
                $url,
                );
            return json_decode($jsonCustomers->getBody()->getContents(), TRUE);
        } catch (ClientException $e) {
            return self::EMPTY_DATA;
        }
    }

    /**
     * @param array $inputs
     * @param string $domain
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function search(array $inputs, string $domain): array
    {
        $url = "{$domain}/api/user/search";
        $method = "POST";

        try {
            $client = new Client(
                [RequestOptions::VERIFY => FALSE]
            );
            $jsonCustomers = $client->request(
                $method,
                $url,
                [
                    'form_params' => [
                        'inputs' => $inputs
                    ]
                ]
            );

            return json_decode($jsonCustomers->getBody()->getContents(), TRUE);
        } catch (ClientException $e) {
            return self::EMPTY_DATA;
        }
    }

    /**
     * @param int $id
     * @param string $domain
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function find(int $id, string $domain): array
    {
        $url = "{$domain}/api/user";
        $method = 'POST';

        $client = new Client(
            [RequestOptions::VERIFY => FALSE]
        );
        $jsonCustomer = $client->request(
            $method,
            $url,
            [
                'form_params' => [
                    'id' => $id
                ]
            ]
        );
        return json_decode($jsonCustomer->getBody()->getContents(), TRUE);
    }

    /**
     * @param array $inputs
     * @param string $domain
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function update(array $inputs, string $domain): array
    {
        $url = "{$domain}/api/user/update";
        $method = 'POST';

        $client = new Client(
            [RequestOptions::VERIFY => FALSE]
        );
        $jsonResponse = $client->request(
            $method,
            $url,
            [
                'form_params' => [
                    'inputs' => $inputs
                ]
            ]
        );
        return json_decode($jsonResponse->getBody()->getContents(), TRUE);
    }

    /**
     * @param int $id
     * @param string $domain
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function destroy(int $id, string $domain)
    {
        $url = "{$domain}/api/user/destroy";
        $method = 'POST';

        $client = new Client(
            [RequestOptions::VERIFY => FALSE]
        );
        $jsonResponse = $client->request(
            $method,
            $url,
            [
                'form_params' => [
                    'id' => $id
                ]
            ]
        );
        return json_decode($jsonResponse->getBody()->getContents(), TRUE);
    }

    public function getArchivedList(string $domain)
    {
        $url = "{$domain}/api/user/collection/archived";
        $method = "POST";

        try {
            $client = new Client(
                [RequestOptions::VERIFY => FALSE]
            );
            $jsonCustomers = $client->request(
                $method,
                $url,
                );
            return json_decode($jsonCustomers->getBody()->getContents(), TRUE);
        } catch (ClientException $e) {
            return self::EMPTY_DATA;
        }
    }
}
