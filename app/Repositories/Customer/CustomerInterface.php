<?php


namespace App\Repositories\Customer;

/**
 * Interface CustomerInterface
 * @package App\Repositories\Customer
 */
interface CustomerInterface
{
    /**
     * @constant
     */
    const EMPTY_DATA = ['data' => []];

    /**
     * @param string $domain
     * @return array
     */
    public function all(string $domain): array;

    /**
     * @param array $inputs
     * @param string $domain
     * @return array
     */
    public function search(array $inputs, string $domain): array;

    /**
     * @param int $id
     * @param string $domain
     * @return array
     */
    public function find(int $id, string $domain): array;

    /**
     * @param array $inputs
     * @param string $domain
     * @return array
     */
    public function update(array $inputs, string $domain): array;
}
