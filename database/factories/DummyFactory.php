<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Content;
use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->afterCreating(App\Models\User::class, function ($user, Faker $faker) {
    for($i=0; $i<=2; $i++)
    {
        $copyTargetFileNamePath = 'test/DummyFile.zip';
        $dummyFileName = 'DummyFile_' . $faker->unixTime . $faker->randomNumber() . '.zip';
        $dummyFileNamePath = 'public/contents/' . $user->id . '/' . $dummyFileName;
        Storage::copy($copyTargetFileNamePath, $dummyFileNamePath);
        factory(App\Models\Content::class)->create(['user_id' => $user->id, 'content_url' => $dummyFileNamePath]);
    }
});

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'domain' => $faker->domainName,
        'remember_token' => Str::random(10),
    ];
});

$factory->define(Content::class, function (Faker $faker) {
    return [
        'user_id' => 0,
        'content_url' => storage_path('app/public/contents'),
        'release_days' => $faker->numberBetween(0, 60),
        'name' => "{$faker->word}{$faker->word}サービス",
        'description' => $faker->sentence,
        'product_key' => $faker->uuid,
    ];
});
