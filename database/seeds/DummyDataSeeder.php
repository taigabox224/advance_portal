<?php

use Illuminate\Database\Seeder;
use \App\Models\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class DummyDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Storage::deleteDirectory("public/contents");
        DB::table('users')->delete();

        $env = \App::environment();

        switch ($env) {
            case 'testing':
                $domain = 'https://phoenix-2020-testing.herokuapp.com';
                break;
            default:
                $domain = 'https://phoenix.dev';
                break;
        }

        User::create([
            'name' => 'okada',
            'email' => 'okada@org',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'domain' => $domain,
            'remember_token' => Str::random(10),
        ]);

        factory(User::class, 5)->create();
    }
}
