<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::Group(['middleware' => ['api'], 'prefix' => 'content', 'namespace' => 'Content'], function () {
    Route::post('', 'ContentAPIController@content')->name('api.content');
    Route::post('collection', 'ContentAPIController@contentCollection')->name('api.content.collection');
    Route::post('download', 'ContentAPIController@download')->name('api.content.download');
});

Route::post('/api_test', 'TestController@test')->name('api.test');
