@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        <form action="{{ route('api.test') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label for="subway_api_key" class="col-md-4 col-form-label text-md-right"></label>
                                <div class="col-md-6">
                                    <input type="hidden" class="form-control" id="subway_api_key" name="subway_api_key"
                                           value="{{ config('app.subway_api_key') }}">
                                </div>
                            </div>
                            <input type="submit" value="送信">
                        </form>

                        <form action="{{ route('api.content.collection') }}" method="post"
                              enctype="multipart/form-data">
                            @csrf

                            <input type="hidden" class="form-control" id="subway_api_key" name="subway_api_key"
                                   value="{{ config('app.subway_api_key') }}">
                            <input type="hidden" class="form-control" id="created_at" name="created_at" value="2020-06-01 02:44:21">
                            <input type="hidden" class="form-control" id="bonus_point" name="bonus_point" value=30>

                            <div class="form-group row">
                                <label for="user_id" class="col-md-4 col-form-label text-md-right">ID</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="user_id" name="user_id">
                                </div>
                            </div>
                            <input type="submit" value="送信">
                        </form>

                        <form action="{{ route('api.content') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label for="subway_api_key" class="col-md-4 col-form-label text-md-right"></label>
                                <div class="col-md-6">
                                    <input type="hidden" class="form-control" id="subway_api_key" name="subway_api_key"
                                           value="{{ config('app.subway_api_key') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="id" class="col-md-4 col-form-label text-md-right">ID</label>
                                <div class="col-md-6">
                                    <input type="number" class="form-control" id="id" name="id">
                                </div>
                            </div>
                            <input type="submit" value="送信">
                        </form>

                        <form action="{{ route('api.content.download') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label for="subway_api_key" class="col-md-4 col-form-label text-md-right"></label>
                                <div class="col-md-6">
                                    <input type="hidden" class="form-control" id="subway_api_key" name="subway_api_key"
                                           value="{{ config('app.subway_api_key') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="content_url" class="col-md-4 col-form-label text-md-right">コンテンツURL</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="content_url" name="content_url">
                                </div>
                            </div>
                            <input type="submit" value="送信">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
