@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @component('components.session_alert')
                @endcomponent
                <div class="card">
                    <div class="card-header">コンテンツのアップロード</div>

                    <div class="card-body">
                        <form action="{{ route('content.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label for="content" class="col-md-4 col-form-label text-md-right">アップロードファイル</label>
                                <div class="col-md-6">
                                    <input type="file" class="form-control-file @error('file') is-invalid @enderror"
                                           id="content" name="file">
                                    @error('file')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">コンテンツ名</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control @error('name') is-invalid @enderror"
                                           id="name" name="name" value="{{ old('name') }}">
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">コンテンツ概要</label>
                                <div class="col-md-6">
                                    <textarea class="form-control @error('description') is-invalid @enderror"
                                              id="description" name="description"
                                              rows=5>{{ old('description') }}</textarea>
                                    @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="releaseDays" class="col-md-4 col-form-label text-md-right">解禁日数</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control @error('release_days') is-invalid @enderror"
                                           id="releaseDays" name="release_days" value="{{ old('release_days') }}">
                                    @error('release_days')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-md-4 col-form-label text-md-right"></label>
                                <div class="col-md-6">
                                    <input type="submit" class="btn btn-primary" value="送信">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
