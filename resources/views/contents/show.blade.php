@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @component('components.session_alert')
                @endcomponent
                <div class="card">
                    <div class="card-header">コンテンツ詳細</div>

                    <div class="card-body">

                        <a href="{{ route('content.edit', ['content' => $content]) }}" class="btn btn-outline-secondary mb-3">編集する</a>

                        <table class="table">
                            <tr>
                                <th scope="col" style="width:25%">コンテンツ名</th>
                                <td>{{ $content->name }}</td>
                            </tr>
                            <tr>
                                <th scope="col" style="width:45%">コンテンツ概要</th>
                                <td>{{ $content->description }}</td>
                            </tr>
                            <tr>
                                <th scope="col" style="width:15%">解禁日数</th>
                                <td>{{ $content->release_days }} 日経過</td>
                            </tr>
                            <tr>
                                <th scope="col" style="width:15%">プロダクトキー</th>
                                <td>{{ $content->product_key }}</td>
                            </tr>
                            <tr>
                                <th scope="col" style="width:15%">コンテンツURL</th>
                                <td>{{ $content->content_url }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
