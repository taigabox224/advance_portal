@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @component('components.session_alert')
                @endcomponent
                <div class="card">
                    <div class="card-header">提供中のコンテンツ</div>

                    <div class="card-body">


                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col" style="width:25%">コンテンツ名</th>
                                <th scope="col" style="width:45%">コンテンツ概要</th>
                                <th scope="col" style="width:15%">解禁日数</th>
                                <th scope="col" style="width:15%"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($contents as $content)
                                <tr>
                                    <td>
                                        <a href="{{ route('content.show', ['content' => $content->id]) }}">{{ $content->name }}</a>
                                    </td>
                                    <td>{{ $content->description }}</td>
                                    <td>{{ $content->release_days }}</td>
                                    <td>
                                        <form action="{{ route('content.destroy', ['content' => $content->id]) }}"
                                              method="post" name="delete_content">
                                            @csrf
                                            @method('delete')
                                            <input type="submit" class="btn btn-outline-danger" value="削除"
                                                   name="delete_submit"
                                                   onclick="deleteAlert('delete_content', 'コンテンツを削除してもよろしいでしょうか？');return false">
                                            {{--                                            TODO::削除時のモーダル関数は後々別ファイルに切り出す--}}
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
