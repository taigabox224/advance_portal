@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                    @component('components.session_alert')
                    @endcomponent
                <div class="card">
                    <div class="card-header">コンテンツ編集</div>

                    <div class="card-body">

                        <form action="{{ route('content.update', ['content' => $content->id]) }}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            @method('put')

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">コンテンツ名</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="name" name="name"
                                           value="{{ $content->name }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">コンテンツ概要</label>
                                <div class="col-md-6">
                                    <textarea class="form-control" id="description" name="description"
                                              rows=5>{{ $content->description }}</textarea>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="releaseDays" class="col-md-4 col-form-label text-md-right">解禁日数</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="releaseDays" name="release_days"
                                           value="{{ $content->release_days }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="product_key" class="col-md-4 col-form-label text-md-right">プロダクトキー</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="productKey" name="product_key"
                                           value="{{ $content->product_key }}" disabled>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="content_url" class="col-md-4 col-form-label text-md-right">コンテンツURL</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="contentUrl" name="content_url"
                                           value="{{ $content->content_url }}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-md-4 col-form-label text-md-right"></label>
                                <div class="col-md-6">
                                    <input type="submit" class="btn btn-primary" value="送信">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
