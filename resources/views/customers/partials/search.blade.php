<div class="collapse" id="collapseExample">
    <form action="{{ route('customer.search') }}" method="post">
        @csrf

        <div class="form-row">
            {{-- 開始日順ソート --}}
            <div class="form-group col-md-4">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="sort_service_start_date" id="sort" value="asc"
                           @if(session()->has('old.sort_service_start_date')) @if(session('old')['sort_service_start_date'] == 'asc') checked @endif @endif>
                    <label class="form-check-label">開始日昇順</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="sort_service_start_date" id="sort" value="desc"
                           @if(session()->has('old.sort_service_start_date')) @if(session('old')['sort_service_start_date'] == 'desc') checked
                           @endif @else checked @endif>
                    <label class="form-check-label">開始日降順</label>
                </div>
            </div>
        </div>

        <div class="form-row">
            {{-- 顧客名 --}}
            <div class="form-group col-md-4">
                <label for="name" class="col-form-label text-md-right">顧客名</label>
                <input type="text" class="form-control" id="name" name="name"
                       @if(session()->has('old.name')) value="{{ session('old')['name'] }}" @endif>
            </div>

            {{-- LINE名 --}}
            <div class="form-group col-md-4">
                <label for="name" class="col-form-label text-md-right">LINE名</label>
                <input type="text" class="form-control" id="lineName" name="line_name"
                       @if(session()->has('old.line_name')) value="{{ session('old')['line_name'] }}" @endif>
            </div>

            {{-- Eメール --}}
            <div class="form-group col-md-4">
                <label for="email" class="col-form-label text-md-right">Eメール</label>
                <input type="text" class="form-control" id="email" name="email"
                       @if(session()->has('old.email')) value="{{ session('old')['email'] }}" @endif>
            </div>
        </div>

        <div class="form-row">
            {{-- お支払い方法 --}}
            <div class="form-group col-md-4">
                <label for="payment" class=" col-form-label text-md-right">お支払い方法</label>

                <select class="form-control" name="payment">
                    <option value="">--指定なし--</option>
                    <option value="0"
                            @if(session()->has('old.payment')) @if(session('old')['payment'] == 0) selected @endif @endif>
                        クレジットカード
                    </option>
                    <option value="1"
                            @if(session()->has('old.payment')) @if(session('old')['payment'] == 1) selected @endif @endif>
                        銀行振込
                    </option>
                    <option value="2"
                            @if(session()->has('old.payment')) @if(session('old')['payment'] == 2) selected @endif @endif>
                        TAKETIN
                    </option>
                    <option value="3"
                            @if(session()->has('old.payment')) @if(session('old')['payment'] == 3) selected @endif @endif>
                        退会済み
                    </option>
                </select>
            </div>

            {{-- お支払い状況 --}}
            <div class="form-group col-md-4">
                <label for="payment_status_bank"
                       class="col-form-label text-md-right">お支払い状況</label>

                <select class="form-control" id="payment_status_bank" name="payment_status">
                    <option value="">--指定なし--</option>
                    <option value=1
                            @if(session()->has('old.payment_status')) @if(session('old')['payment_status'] == 1) selected @endif @endif>
                        支払済
                    </option>
                    <option value=0
                            @if(session()->has('old.payment_status')) @if(session('old')['payment_status'] == 0) selected @endif @endif>
                        未払い
                    </option>
                    <option value=2
                            @if(session()->has('old.payment_status')) @if(session('old')['payment_status'] == 2) selected @endif @endif>
                        未払い(継続)
                    </option>
                    <option value=3
                            @if(session()->has('old.payment_status')) @if(session('old')['payment_status'] == 3) selected @endif @endif>
                        前払済
                    </option>
                    <option value=4
                            @if(session()->has('old.payment_status')) @if(session('old')['payment_status'] == 4) selected @endif @endif>
                        停止
                    </option>
                </select>
            </div>

            {{-- サービス開始日 --}}
            <div class="form-group col-md-4">
                <label for="service_start_date" class="col-form-label text-md-right">サービス開始日</label>
                <input type="date" class="form-control" id="serviceStartDate" name="service_start_date"
                       @if(session()->has('old.service_start_date')) value="{{ session('old')['service_start_date'] }}" @endif>
            </div>

        </div>

        <div class="form-row">
            {{-- ボーナス--}}
            <div class="form-group col-md-4">
                <label for="bonus_point" class="col-form-label text-md-right">ボーナス</label>
                <input type="number" class="form-control" id="bonus_point" name="bonus_point"
                       @if(session()->has('old.bonus_point')) value="{{ session('old')['bonus_point'] }}" @endif>
            </div>
            {{-- ボーナス検索条件--}}
            <div class="form-group col-md-4">
                <label for="bonus_point" class="col-form-label text-md-right">ボーナス検索条件</label>
                <select class="form-control" id="bonus_point_operator" name="bonus_point_operator">
                    <option value="">--指定なし--</option>
                    <option value="="
                            @if(session()->has('old.bonus_point_operator')) @if(session('old')['bonus_point_operator'] == "=") selected @endif @endif>
                        等しい
                    </option>
                    <option value="<>"
                            @if(session()->has('old.bonus_point_operator')) @if(session('old')['bonus_point_operator'] == "<>") selected @endif @endif>
                        ではない
                    </option>
                    <option value=">="
                            @if(session()->has('old.bonus_point_operator')) @if(session('old')['bonus_point_operator'] == ">=") selected @endif @endif>
                        以上
                    </option>
                    <option value=">"
                            @if(session()->has('old.bonus_point_operator')) @if(session('old')['bonus_point_operator'] == ">") selected @endif @endif>
                        より大きい
                    </option>
                    <option value="<="
                            @if(session()->has('old.bonus_point_operator')) @if(session('old')['bonus_point_operator'] == "<=") selected @endif @endif>
                        以下
                    </option>
                    <option value="<"
                            @if(session()->has('old.bonus_point_operator')) @if(session('old')['bonus_point_operator'] == "<") selected @endif @endif>
                        未満
                    </option>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="" class="col-form-label text-md-right"></label>
            <div class="col-md-6">
                <input type="submit" class="btn btn-primary" value="上記の条件で検索する">
            </div>
        </div>
    </form>
</div>

