@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                @component('components.session_alert')
                @endcomponent
                <div class="card">
                    <div class="card-header">顧客一覧</div>
                    <div class="card-body">

                        @empty($archived)
                            <p>現在のアクティブ顧客数：{{ count($customers['data']) }}名</p>
                        @else
                            <p>現在のアーカイブ済み顧客数：{{ count($customers['data']) }}名</p>
                        @endempty

                        {{-- ボタンメニュー --}}

                        @empty($archived)
                            <div class="d-inline-block">
                                <form action="{{ route('customer.getArchivedList') }}" method="post">
                                    @csrf
                                    <input type="submit" class="btn btn-outline-danger" value="アーカイブ表示">
                                </form>
                            </div>
                        @else
                            <a href="{{ route('customer.index') }}" class="btn btn-outline-secondary">アクティブ顧客表示</a>
                        @endempty


                        <div class="mb-3 d-inline-block" id="buttonMenu">
                            <button type="button" class="btn btn-secondary" data-toggle="collapse"
                                    data-target="#collapseExample"
                                    aria-expanded="false" aria-controls="collapseExample">
                                検索メニュー
                            </button>
                        </div>

                        {{-- 検索メニュー --}}
                        @component('customers.partials.search')
                        @endcomponent

                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col" style="width:15%">顧客名</th>
                                <th scope="col" style="width:15%">LINE名</th>
                                <th scope="col" style="width:10%">Eメール</th>
                                <th scope="col" style="width:15%">支払方法</th>
                                <th scope="col" style="width:15%">支払状況</th>
                                <th scope="col" style="width:15%">ボーナス</th>
                                <th scope="col" style="width:15%">開始日</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($customers['data'] as $customer)
                                <tr>
                                    @empty($archived)
                                        <td>
                                            <a href="{{ route('customer.show', ['customer' => $customer['id']]) }}">{{ $customer['name'] }}</a>
                                        </td>
                                    @else
                                        <td>
                                            {{ $customer['name'] }}
                                        </td>
                                    @endempty
                                    <td>{{ $customer['line_name'] }}</td>
                                    <td>{{ $customer['email'] }}</td>
                                    {{--                                    TODO::ビューコンポーザで加工したい--}}
                                    @if($customer['payment'] == 0)
                                        <td>クレジット</td>
                                    @elseif($customer['payment'] == 1)
                                        <td>銀行振込</td>
                                    @elseif($customer['payment'] == 2)
                                        <td>TAKETIN</td>
                                    @elseif($customer['payment'] == 3)
                                        <td style="color:red">退会済み</td>
                                    @endif

                                    {{-- 銀振・カードどちらか払ってればOK出す--}}
                                    @if($customer['payment_status_bank'] == 1)
                                        <td style="color:blue;">支払済</td>
                                    @elseif($customer['payment_status_bank'] == 0)
                                        <td style="color:red;">未払い</td>
                                    @elseif($customer['payment_status_bank'] == 2)
                                        <td style="color:red;">未払い(継続)</td>
                                    @elseif($customer['payment_status_bank'] == 3)
                                        <td style="color:green;">前払済</td>
                                    @elseif($customer['payment_status_bank'] == 4)
                                        <td style="color:gray;">停止</td>
                                    @endif

                                    <td>{{ $customer['bonus_point'] }}</td>
                                    <td>@if($customer['service_start_date']) {{ (new DateTime($customer['service_start_date']))->format('Y年m月d日') }} @else
                                            待機中 @endif</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
