@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @component('components.session_alert')
                @endcomponent
                <div class="card">
                    <div class="card-header">顧客詳細</div>
                    <div class="card-body">
                        <a href="{{ route('customer.edit', ['customer' => $customer['id']]) }}"
                           class="btn btn-outline-secondary mb-3">編集する</a>
                        <div class="d-inline-block">
                            <form action="{{ route('customer.destroy', ['customer' => $customer['id']]) }}"
                                  method="post" name="archive">
                                @csrf
                                @method('delete')
                                <button class="btn btn-outline-danger ml-1 mb-3" onclick="deleteAlert('archive', '顧客をアーカイブ化してよろしいでしょうか？'); return false">アーカイブ</button>
                            </form>
                        </div>

                        <table class="table">
                            <tr>
                                <th scope="col" style="width:25%">顧客名</th>
                                <td>{{ $customer['name'] }}</td>
                            </tr>
                            <tr>
                                <th scope="col" style="width:25%">LINE名</th>
                                <td>{{ $customer['line_name'] }}</td>
                            </tr>
                            <tr>
                                <th scope="col" style="width:45%">Eメール</th>
                                <td>{{ $customer['email'] }}</td>
                            </tr>
                            <tr>
                                <th scope="col" style="width:15%">お支払い方法</th>
                                @if($customer['payment'] == 0)
                                    <td>クレジット</td>
                                @elseif($customer['payment'] == 1)
                                    <td>銀行振込</td>
                                @elseif($customer['payment'] == 2)
                                    <td>TAKETIN</td>
                                @elseif($customer['payment'] == 3)
                                    <td>退会済み</td>
                                @endif
                            </tr>
                            <tr>
                                <th scope="col" style="width:15%">お支払い状況(クレジットカード)</th>
                                @if($customer['payment_status_card'] == TRUE)
                                    <td style="color:blue;">支払済</td>
                                @elseif($customer['payment_status_card'] == FALSE)
                                    <td style="color:red;">未払い</td>
                                @endif
                            </tr>
                            <tr>
                                <th scope="col" style="width:15%">お支払い状況(銀行振込)</th>
                                @if($customer['payment_status_bank'] == 1)
                                    <td style="color:blue;">支払済</td>
                                @elseif($customer['payment_status_bank'] == 0)
                                    <td style="color:red;">未払い</td>
                                @elseif($customer['payment_status_bank'] == 2)
                                    <td style="color:red;">未払い(継続)</td>
                                @elseif($customer['payment_status_bank'] == 3)
                                    <td style="color:green;">前払済</td>
                                @elseif($customer['payment_status_bank'] == 4)
                                    <td style="color:gray;">停止</td>
                                @endif
                            </tr>
                            <tr>
                                <th scope="col" style="width:15%">ボーナスポイント</th>
                                <td>{{ $customer['bonus_point'] }}</td>
                            </tr>
                            <tr>
                                <th scope="col" style="width:15%">備考欄</th>
                                <td>{{ $customer['marks'] }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
