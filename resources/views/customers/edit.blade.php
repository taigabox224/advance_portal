@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @component('components.session_alert')
                @endcomponent
                <div class="card">
                    <div class="card-header">顧客編集</div>

                    <div class="card-body">
                        @if(is_null($customer['service_start_date']))
                        <div>
                            <form action="{{ route('customer.update', ['customer' => $customer['id']]) }}" method="post">
                                @csrf
                                @method('put')
                                <input type="hidden" class="form-control" id="id" name="id" value="{{ $customer['id'] }}">
                                <input type="hidden" class="form-control" id="serviceStartDate" name="service_start_date" value="{{ \Carbon\Carbon::now() }}">
                                <input type="submit" class="btn btn-success" value="利用開始">
                            </form>
                        </div>
                        @endif

                        <form action="{{ route('customer.update', ['customer' => $customer['id']]) }}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <input type="hidden" class="form-control" id="id" name="id" value="{{ $customer['id'] }}">

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">顧客名</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="name" name="name"
                                           value="{{ $customer['name'] }}" disabled>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="line_name" class="col-md-4 col-form-label text-md-right">LINE名</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="lineName" name="line_name"
                                           value="{{ $customer['line_name'] }}" disabled>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">Eメール</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="email" name="email" value="{{ $customer['email'] }}" disabled>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="payment" class="col-md-4 col-form-label text-md-right">お支払い方法</label>
                                <div class="col-md-6">
                                    <select class="form-control" id="payment" name="payment">
                                        <option value=0 class="form-control" @if($customer['payment'] == 0) selected @endif>クレジットカード</option>
                                        <option value=1 class="form-control" @if($customer['payment'] == 1) selected @endif>銀行振込</option>
                                        <option value=2 class="form-control" @if($customer['payment'] == 2) selected @endif>TAKETIN</option>
                                        <option value=3 class="form-control" @if($customer['payment'] == 3) selected @endif>退会済み</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="payment_status_bank" class="col-md-4 col-form-label text-md-right">お支払い状況(クレジット)</label>
                                <div class="col-md-6">
                                    <select class="form-control" id="payment_status_bank" name="payment_status_bank" disabled>
                                        <option value=1 @if($customer['payment_status_card']) selected @endif>支払済</option>
                                        <option value=0 @if(!$customer['payment_status_card']) selected @endif>未払い</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="payment_status_bank" class="col-md-4 col-form-label text-md-right">お支払い状況(銀行振込)</label>
                                <div class="col-md-6">
                                    <select class="form-control" id="payment_status_bank" name="payment_status_bank">
{{--                                        <option value=1 @if($customer['payment_status_bank']) selected @endif>支払済</option>--}}
{{--                                        <option value=0 @if(!$customer['payment_status_bank']) selected @endif>未払い</option>--}}
                                        <option value=0 @if($customer['payment_status_bank'] == 0) selected @endif>未払い</option>
                                        <option value=1 @if($customer['payment_status_bank'] == 1) selected @endif>支払済</option>
                                        <option value=2 @if($customer['payment_status_bank'] == 2) selected @endif>未払い(継続)</option>
                                        <option value=3 @if($customer['payment_status_bank'] == 3) selected @endif>前払済</option>
                                        <option value=4 @if($customer['payment_status_bank'] == 4) selected @endif>停止</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="bonus_point" class="col-md-4 col-form-label text-md-right">ボーナス</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="bonus_point" name="bonus_point"
                                           value="{{ $customer['bonus_point'] }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="marks" class="col-md-4 col-form-label text-md-right">備考欄</label>
                                <div class="col-md-6">
                                    <textarea type="text" class="form-control" id="marks" name="marks">{{ $customer['marks'] }}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <input type="hidden" class="form-control" id="serviceStartDate" name="service_start_date"
                                           value="{{ $customer['service_start_date'] }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-md-4 col-form-label text-md-right"></label>
                                <div class="col-md-6">
                                    <input type="submit" class="btn btn-primary" value="送信">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
