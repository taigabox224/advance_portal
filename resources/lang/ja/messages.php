<?php

return [
    'success' => ':target の処理は正常に処理されました',
    'failure' => ':target の処理は中断されました'
];
